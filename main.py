import pandas as pd
from sklearn import model_selection
from apyori import apriori

def load_data(filename):
    data = pd.read_csv(filename)
    return data
    # print(data)

data = load_data('store_data')
data = load_data('store_data2')
item_list = data.unstack().dropna().unique()
print(item_list)
print("#items: ",len(item_list))
train, test = model_selection.train_test_split(data, test_size=.10)
print("train: ", train)
print("test: ",test)

train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
print("transform")
# print(train)
for i in train[:10]:
    print(i)

result = apriori(train)
print(result)



